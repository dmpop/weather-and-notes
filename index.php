<?php
include('config.php');
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $title ?></title>
	<link rel="shortcut icon" href="favicon.png" />
	<link rel="stylesheet" href="lit.css">
	<link href="https://fonts.googleapis.com/css2?family=Barlow" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1><?php echo $title ?></h1>
	    <hr>
	    <p><a href="w.php">Weather by geolocation</a> Obtain and weather info using geolocation.</p>
	    <p><a href="wn.php">Weather and notes</a> Get weather info by specifying city and country. You can add notes too.</p>
	    <p><a href="view.php">View</a> View saved data.</p>
	</div>
    </body>
</html>
