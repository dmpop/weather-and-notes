# Weather and notes

Weather and notes is a simple PHP script that obtains current weather info for the given location and save it long with an optional note in separate text file.

The accompanying shell script can be used to read the date of a specified photo then find a matching text file, and write its contents into the _Comment_ field of the photo.

## Author

Dmitri Popov [dmpop@linux.com](mailto:dmpop@linux.com)

## License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)
