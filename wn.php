<?php
include('config.php');
if ($protect) {
    require_once('protect.php');
}
if (empty($api_key)) {
    exit("API key is not provided.");
}
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $title ?></title>
	<link rel="shortcut icon" href="favicon.png" />
	<link rel="stylesheet" href="lit.css">
	<link href="https://fonts.googleapis.com/css2?family=Barlow" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1><?php echo $title ?></h1>
	    <hr>
	    <form method='get' action=''>
		<label for='note'>Note: </label><br>
		<textarea class="card w-100" style="height: 25em; line-height: 1.9;" name="note"></textarea><br>
		<input type="checkbox" name="include">
		<label for='include'>Include weather</label><br>
		<label for='city'>City: </label><br>
	    <input type='text' name='city'><br>
	    <label for='city'>Country code: </label><br>
	    <input type='text' name='country'><br>
	    <button class="btn primary" style='margin-top: 0.5em;' type='submit' role='button' name='submit'>Save</button>
	</form>
	<?php
	echo "API provider: ".$api."<br>";
	if (!file_exists("data")) {
	    mkdir("data", 0777, true);
	}
	$date = date('Y-m-d');
	if(!empty($_GET["note"])) {
	    $note = $_GET["note"];
	} else {
	    $note = "";
	}
	if(!empty($_GET["include"])) {
	    if(!empty($_GET["city"]) && !empty($_GET["country"])) {
		$city = $_GET["city"];
		$country = $_GET["country"];
	    } else {
		exit("Specify city and country.");
	    }
	    if ($api == "openweathermap") {
		$request = "http://api.openweathermap.org/data/2.5/weather?APPID=$api_key&q=$city,$country&units=metric&cnt=7&lang=en&units=metric&cnt=7&lang=en";
	    }
	    if ($api == "weatherbit") {
		$request = "https://api.weatherbit.io/v2.0/current?city=".$city.",".$country."&key=".$api_key;
	    }
	    $response = file_get_contents($request);
	    $data = json_decode($response,true);
	    echo "<h2>Weather in ".$city.", ".$country."</h2>";
	    if ($api == "openweathermap") {
		$temp = $data['main']['temp_max'];
		$weather = $data['weather'][0]['main'];
		$wind = $data['wind']['speed'];
	    }
	    if ($api == "weatherbit")
	    {
		$temp = $data['data']['0']['temp'];
		$weather = $data['data']['0']['weather']['description'];
		$wind = $data['data']['0']['wind_spd'];
	    }
	    echo "Temperature: ".$temp."°C<br>";
	    echo "Current conditions: ".$weather."<br>";
	    echo "Wind speed: ".$wind."m/s<br>";
	    $f = fopen("data/".$date.".txt", "a");
	    fwrite($f,$city." (".$country.") ".$weather.", ".$temp."°C, ".$wind."m/s. ".$note."\n");
	    fclose($f);
	} else {
	    if(isset($_GET['submit'])) {
		$f = fopen("data/".$date.".txt", "a");
		fwrite($f,$note."\n");
		fclose($f);

	    }
	}
	?>
	<p><a href="view.php">View</a></p>
    </body>
</html>
