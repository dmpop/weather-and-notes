<?php
include('config.php');
if ($protect) {
    require_once('protect.php');
}
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $title ?></title>
	<link rel="shortcut icon" href="favicon.png" />
	<link rel="stylesheet" href="lit.css">
	<link href="https://fonts.googleapis.com/css2?family=Barlow" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1><?php echo $title ?></h1>
	    <a class="btn" href="index.php">Back</a>
	    <?php
	    //$files = scandir("data");
	    //Get a list of file paths using the glob function.
	    $flist = array_reverse(glob('data/*.txt'));
	    //Loop through the array that glob returned.
	    foreach($flist as $f){
		$fname = basename($f, ".txt");
		echo "<h2>".$fname."</h2>";
		echo file_get_contents($f, true);
		echo "<br>";
	    }
	    ?>
	</div>
    </body>
</html>
