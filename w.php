<?php
require_once('protect.php');
$title = "Weather by geolocation";
$weatherbit_api_key = "ee1592672ae1423b9bb92919e2b51e82";
?>
<html lang="en">
    <!-- Author: Dmitri Popov, dmpop@linux.com
	 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $title ?></title>
	<link rel="shortcut icon" href="favicon.png" />
	<link rel="stylesheet" href="lit.css">
	<link href="https://fonts.googleapis.com/css2?family=Barlow" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
	<div class="c">
	    <h1><?php echo $title ?></h1>
	    <hr>
	    <button class="btn primary" onclick="getLocation()">Get coordinates</button>
	    <p id="geolocation"></p>
	    <script>
	     var x = document.getElementById("geolocation");
	     function getLocation() {
		 if (navigator.geolocation) {
		     navigator.geolocation.getCurrentPosition(showPosition);
		 } else { 
		     x.innerHTML = "Geolocation is not supported by this browser.";
		 }
	     }
	     function showPosition(position) {
		 x.innerHTML = "Latitude: " + position.coords.latitude + 
			       "<br>Longitude: " + position.coords.longitude;
		 document.cookie = "posLat = ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
		 document.cookie = "posLon = ; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
		 document.cookie = "posLat = "+ position.coords.latitude;
		 document.cookie = "posLon = "+ position.coords.longitude;
	     }
	    </script>
	    <?php
	    if (!file_exists("data")) {
		mkdir("data", 0777, true);
	    }
	    $date = date('Y-m-d');
	    if(!empty($_POST["note"])) {
		$note = $_POST["note"];
	    } else {
		$note = "";
	    }
	    if(isset($_POST['save'])) {
		$lat = $_COOKIE['posLat'];
		$lon = $_COOKIE['posLon'];
		$request = "https://api.weatherbit.io/v2.0/current?lat=".$lat."&lon=".$lon."&key=".$weatherbit_api_key;
		$response = file_get_contents($request);
		$data = json_decode($response,true);
		$city = $data['data']['0']['city_name'];
		$country = $data['data']['0']['country_code'];
		$temp = $data['data']['0']['temp'];
		$weather = $data['data']['0']['weather']['description'];
		$wind = floor($data['data']['0']['wind_spd'] * 100) / 100;
		echo "Temperature: ".$temp."°C<br>";
		echo "Current conditions: ".$weather."<br>";
		echo "Wind speed: ".$wind."m/s<br>";
		$f = fopen("data/".$date.".txt", "a");
		fwrite($f,$city." (".$country.") ".$weather.", ".$temp."°C, ".$wind."m/s.\n");
		fclose($f);
	    }
	    ?>
	    <form method='post' action=''>
		<label for='note'>Note: </label>
		<textarea class="card w-100" style="height: 25em; line-height: 1.9;" name="note"></textarea>
		<button class="btn primary" style='margin-top: 1.5em;' type='submit' role='button' name='save'>Save</button>
	    </form>
    </body>
</html>
